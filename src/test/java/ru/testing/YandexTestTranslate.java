package ru.testing;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class YandexTestTranslate extends Maine{
    @Test
    public void CorrectTranslation() throws IOException {
        String string = YandexTestTranslate.main("Hello World!", "trnsl.1.1.20140730T093341Z.2c7b04a1b3c46ffa.9172b5a848a230604cf5fb0de835e3caead26a7e" );
        Assertions.assertEquals(string,"Всем Привет!");
    }

}
