package ru.testing;
import javax.net.ssl.HttpsURLConnection;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

public class Maine {
    public static String main(String phrase, String apiKey) throws IOException {
        Scanner sc = new Scanner(System.in);
        String translate;
        while ((translate = sc.next()) != null) {
            String urlStr = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20140730T093341Z.2c7b04a1b3c46ffa.9172b5a848a230604cf5fb0de835e3caead26a7e";
            URL url = new URL(urlStr);
            HttpsURLConnection connection = (HttpsURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
            dataOutputStream.writeBytes("text=" + URLEncoder.encode(translate, "UTF-8") + "&lang=ru");

            InputStream response = connection.getInputStream();
            String json = new java.util.Scanner(response).nextLine();
            int start = json.indexOf("[");
            int end = json.indexOf("]");
            String translated = json.substring(start + 2, end - 1);
            System.out.println(translated);
            connection.disconnect();
        }
        return translate;
    }
}
